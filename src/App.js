import React from 'react';
import './App.css';
import { Sketch } from './Sketch';

function App() {

    return (
        <>
            <Sketch />
        </>
    )
}
export default App;